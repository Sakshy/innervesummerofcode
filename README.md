# ISOC#

Innerve summer of code was a competition organised by IGDTUW, Delhi. In this competition, participants were required to contribute in making a website for a college Innerve Institute of Technology. Innerve was their college fest. Preffered tech stack was HTML, CSS and JS. But fullstack websites were also accepted. 

### What is in this repository? ###

* Admin and Student login page template
* Events page
* Forgot Password page
* News bulletin

### How do I get set up? ###

* Run each file one by one to access the pages 